import datetime

from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from users.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        # Запускаешь один раз
        print(datetime.datetime.now())

        g, created = Group.objects.get_or_create(name='consultants')
        for i in range(5):
            user = User.objects.create_user(username=g.name + '-user-' + str(i),
                                 email=g.name + str(i) + '@gmail.com',
                                 password='123456789m')
            g.user_set.add(user)
