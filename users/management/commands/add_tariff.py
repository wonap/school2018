import json
import datetime

from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from users.models import User

class Command(BaseCommand):

    def handle(self, *args, **options):
        # Запускаешь один раз
        print(datetime.datetime.now())
        tarif_groups = ['food', 'food_plus_sport',
                        'food_plus_specialist', 'pregnancy',
                        'all_inclusive']
        for tg in tarif_groups:
            g, created = Group.objects.get_or_create(name=tg)
            for i in range(2):
                user = User.objects.create_user(username=g.name + '-user-' + str(i),
                                 email=g.name + str(i) + '@gmail.com',
                                 password='123456789m')
                g.user_set.add(user)

