from training.models import Training
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):

    def handle(self, *args, **options):
        content_type = ContentType.objects.get_for_model(Training)
        permission = Permission.objects.create(codename='can_view',
                                       name='Can View Training',
                                       content_type=content_type)
