from django.conf.urls import url
from . import views

app_name = 'users'

urlpatterns = [
    url(r'^profile/edit/$', views.UserProfileEditView.as_view(), name='profile-edit'),
    url(r'^profile/add_in_group/$', views.UserAddToInGroupView.as_view(), name='add-in-group'),
    url(r'^profile/family/edit/(?P<pk>[0-9]+)/$', views.UserProfileFamilyEditView.as_view(), name='profile-family-edit'),
    url(r'^profile/$', views.UserProfileView.as_view(), name='profile'),
    url(r'^profile/password-edit/$', views.UserPasswordEditView.as_view(), name='password-edit'),
    url(r'^all/$', views.users_list, name='users-list'),
]
