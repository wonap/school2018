from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect
from django.views.generic import DetailView, View,TemplateView
from django.core.urlresolvers import reverse_lazy

from common.models import KnowledgeLevel
from common.mixins import PayCheck
from users.forms import UserEditForm,UserFamilyEditForm, AddUserInGroupForm
from .models import User, Family


class UserProfileEditView(LoginRequiredMixin, PayCheck, View):
    template_name = 'users/profile_edit.html'

    def get(self, request, *args, **kwargs):
        user = request.user
        form = UserEditForm(instance=user)
        user_weight = user.weight
        ctx = dict()
        ctx['form'] = form
        ctx['user'] = user

        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        form = UserEditForm(request.POST,request.FILES, instance=request.user)
        if form.is_valid():
            user = form.save()
            return redirect(reverse_lazy('users:profile'))
        else:
            return render(request, self.template_name,{
                'form': form,
                'user': request.user,
            })


class UserProfileFamilyEditView(LoginRequiredMixin, View):
    template_name = 'users/profile_family_edit.html'

    def get(self, request, *args, **kwargs):
        user = User.objects.get(pk=kwargs.get('pk'))
        form = UserFamilyEditForm(instance=user)
        ctx = dict()
        ctx['form'] = form
        return render(request,self.template_name,ctx)

    def post(self, request, *args, **kwargs):
        user = User.objects.get(pk=kwargs.get('pk'))
        form = UserFamilyEditForm(request.POST,request.FILES, instance=user)
        if form.is_valid():
            user = form.save()
            return redirect(reverse_lazy('users:profile'))
        else:
            return render(request, self.template_name,{
                'form': form,
                'user': request.user,
            })


class UserProfileView(LoginRequiredMixin, View):
    template_name = 'users/profile.html'

    def get_kbj(self, user):
        user_attrs = [user.weight, user.height, user.age,
                      user.gender, user.lifestyle, user.goal]
        if all(user_attrs):
            if user.gender == 'F':
                voo = user.weight * 9.99 + 6.25 * user.height - 4.92 * user.age - 161
            elif user.gender == 'M':
                voo = user.weight * 9.99 + 6.25 * user.height - 4.92 * user.age + 5

            voo = voo * user.lifestyle.coef
            if user.goal == 'lose':
                res = (voo/100) * 20
                res_voo = voo - res
            elif user.goal == 'gain':
                res = (voo/100) * 20
                res_voo = voo + res
            elif user.goal == 'maintain':
                res_voo = voo
            proteins = (res_voo/100) * 30
            fats = (res_voo/100) * 30
            carbs = (res_voo/100) * 40
            proteins = proteins / 4
            fats = fats / 8
            carbs = carbs / 4

            results = {
                'proteins' : proteins,
                'fats': fats,
                'carbs': carbs,
                'calories': res_voo,
            }
        else:
            results = {
                'proteins': 0,
                'fats': 0,
                'carbs': 0,
                'calories': 0,
            }

        return results

    def get(self, request, *args, **kwargs):
        ctx = dict()
        ctx['results'] = self.get_kbj(request.user)
        if request.user.is_family_user():
            ctx['family_users'] = request.user.get_family_users()
        elif request.user.is_children_user():
            ctx['family_users'] = request.user.get_family_users()

        return render(request, self.template_name, ctx)


class UserAddToInGroupView(LoginRequiredMixin, View):

    template_name ='users/add_user_in_group_family.html'

    def get(self, request, *args, **kwargs):
        form = AddUserInGroupForm()
        ctx = dict()
        ctx['form'] = form
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        print(request.POST)
        form = AddUserInGroupForm(request.POST)
        common_id = request.user.id + 1000
        error = ''
        c = Family.objects.filter(common_id=common_id).count()
        group = request.user.get_tariff()
        if group.name == 'family':
            col = 2
        elif group.name == 'children':
            col = 1

        if c > col:
            error = 'Вы не можете'
        elif form.is_valid():
            user = User()
            user.set_password('159159147')
            email = str(request.user.id) + str(c) + request.user.email
            user.username = email
            user.email = email
            user.first_name = form.cleaned_data.get('first_name')
            user.last_name = form.cleaned_data.get('last_name')

            user.save()
            group.user_set.add(user)

            f = Family()
            f.user = user
            f.common_id = common_id
            f.type = group.name
            f.save()

            f_current = Family.objects.get_or_create(
                user=request.user,
                common_id = common_id,
                type = group.name
            )

            print('=========================================================================')
            print(group)
            return redirect(reverse_lazy('users:profile'))
        ctx = dict()
        ctx['form'] = form
        ctx['error'] = error
        return render(request, self.template_name, ctx)

class UserPasswordEditView(LoginRequiredMixin, TemplateView):
    template_name = 'users/change_password.html'


def users_list(request):
    import csv
    users = []
    with open('users.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            new_user = {
                'email': row[3],
                'tariff': row[1],
                'level': row[2],
            }
            users.append(new_user)

    print(len(users))
    c = 0
    for user in users:
        group = Group.objects.filter(name=user['tariff']).first()
        knowledge = KnowledgeLevel.objects.filter(code=user['level']).first()
        user_yes = User.objects.filter(email=user['email']).first()
        user_yes.knowledge=knowledge
        user_yes.save()
        if not user_yes:
            if group and knowledge:
                n_user = User.objects.create_user(username=user['email'],
                                 email=user['email'],
                                 password='159159147', knowledge=knowledge)
                group.user_set.add(n_user)
                print(group)
                c +=1
            print(c)
    return render(request, 'users/users_list.html', {
        'users': users,
    })
