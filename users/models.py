from django.db import models

from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager,
    AbstractUser, Group, PermissionsMixin)
from django.utils.translation import ugettext_lazy as _

from common.models import KnowledgeLevel


class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None, **kwargs):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(username=username,
                          email=UserManager.normalize_email(email), )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        username = email
        user = self.create_user(username, email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class ContentUserManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(groups__name='content_manager')


class ConsultantUserManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(groups__name='consultants')


class FamilyUserManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(groups__name__in=['family','children'])


class Lifestyle(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50, null=True, blank=True)
    coef = models.FloatField(default=1.0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Образ жизни'
        verbose_name_plural = 'Образ жизни'


class Family(models.Model):
    user = models.ForeignKey('users.User',related_name='family')
    common_id = models.IntegerField(default=0)
    CHILDREN, FAMILY = 'children', 'family'
    TYPE_CHOICES = (
        (CHILDREN, 'Детский'),
        (FAMILY, 'Семейный'),

    )
    type = models.CharField(max_length=30, choices=TYPE_CHOICES, default=FAMILY)

    def __str__(self):
        return str(self.common_id)


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('First name'), max_length=100, blank=True)
    last_name = models.CharField(_('Last name'), max_length=100, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    is_staff = models.BooleanField(_('Staff status'), default=False)
    is_active = models.BooleanField(_('Active'), default=True)
    date_joined = models.DateTimeField(_('date joined'),
                                       default=timezone.now)
    avatar = models.ImageField(upload_to='users', null=True, blank=True)
    instagram = models.CharField(max_length=100, null=True, blank=True)
    MALE, FEMALE = 'M', 'F'
    GENDER_CHOICES = (
        (MALE, 'Мужской'),
        (FEMALE, 'Женский'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default=MALE, blank=True)

    height = models.FloatField(null=True, blank=True)
    weight = models.FloatField(null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)

    LOSEWEIGHT, GAINWEIGHT, MAINTAINWEIGHT = 'lose', 'gain', 'maintain'
    GOAL_CHOICES = (
        (LOSEWEIGHT, 'Сбросить вес'),
        (GAINWEIGHT, 'Набор массы'),
        (MAINTAINWEIGHT, 'Поддерживать вес'),
    )

    lifestyle = models.ForeignKey(Lifestyle, related_name='lifestyle_users', null=True, blank=True)
    goal = models.CharField(max_length=20, choices=GOAL_CHOICES, null=True, blank=True)
    knowledge = models.ForeignKey(KnowledgeLevel, related_name='users', null=True, blank=True)

    GASTROTWO, DIETOLOG, ENDOC, GYNECOLOGIST, GASTRO, PSYCHOLOGIST, NEUROLOGIST, ASSISTENT, GV, PEDIATRICIAN, ZUBAREVA, COACH, DEFAULT = 'gastrotwo', 'dietolog', 'endoc', 'gynecologist', 'gastro', 'psychologist', 'neurologist','assistent', 'gv', 'pediatrician', 'zubareva', 'coach', 'default'
    TYPE_CHOICES = (
        (DEFAULT, 'Пользователь'),
        (COACH, 'Тренер'),
        (ZUBAREVA, 'Зубарева'),
        (PEDIATRICIAN, 'Педиатр'),
        (GV, 'Консультант по ГВ'),
        (ASSISTENT, 'Ассистент'),
        (NEUROLOGIST, 'Невролог'),
        (PSYCHOLOGIST, 'Психолог'),
        (GASTRO, 'Гастроэнтеролог'),
        (GYNECOLOGIST, 'Гинеколог'),
        (ENDOC, 'Эндокринолог'),
        (DIETOLOG, 'Диетолог'),
        (GASTROTWO, 'Гастроэнтеролог 2'),
    )
    type = models.CharField(max_length=30, choices=TYPE_CHOICES, default=DEFAULT)
    last_message_time = models.DateTimeField(null=True, blank=True)
    objects = UserManager()
    content_manager_objects = ContentUserManager()
    consultants_objects = ConsultantUserManager()
    family_objects = FamilyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        permissions = (
            ("can_list", "Просмотр пользователей"),
        )

    def get_kbj(self):
        user_attrs = [self.weight, self.height, self.age,
                      self.gender, self.lifestyle, self.goal]
        if all(user_attrs):
            if self.gender == 'F':
                voo = self.weight * 9.99 + 6.25 * self.height - 4.92 * self.age - 161
            elif self.gender == 'M':
                voo = self.weight * 9.99 + 6.25 * self.height - 4.92 * self.age + 5

            voo = voo * self.lifestyle.coef
            if self.goal == 'lose':
                res = (voo/100) * 20
                res_voo = voo - res
            elif self.goal == 'gain':
                res = (voo/100) * 20
                res_voo = voo + res
            elif self.goal == 'maintain':
                res_voo = voo
            proteins = (res_voo/100) * 30
            fats = (res_voo/100) * 30
            carbs = (res_voo/100) * 40
            proteins = proteins / 4
            fats = fats / 8
            carbs = carbs / 4

            results = {
                'proteins' : proteins,
                'fats': fats,
                'carbs': carbs,
                'calories': res_voo,
            }
        else:
            results = {
                'proteins': 0,
                'fats': 0,
                'carbs': 0,
                'calories': 0,
            }

        return results

    def get_full_name(self):
        if all([self.first_name, self.last_name]):
            full_name = '{0} {1}'.format(self.first_name, self.last_name)
            return full_name.strip()
        else:
            return self.email

    def get_short_name(self):
        return self.first_name

    def get_avatar(self):
        if self.avatar:
            return self.avatar
        else:
            return 'default.jpg'

    def is_content_manager(self):
        return self.groups.filter(name='content_manager').exists()

    def is_consultant_manager(self):
        return self.groups.filter(name='consultants').exists()

    def is_food_user(self):
        return self.groups.filter(name='food').exists()

    def is_food_plus_sport_user(self):
        return self.groups.filter(name='food_plus_sport').exists()

    def is_food_plus_specialist_user(self):
        return self.groups.filter(name='food_plus_specialist').exists()

    def is_pregnancy_user(self):
        return self.groups.filter(name='pregnancy').exists()


    def is_family_user(self):
        return self.groups.filter(name='family').exists()

    def is_children_user(self):
        return self.groups.filter(name='children').exists()

    def is_base_user(self):
        return self.groups.filter(name='base').exists()

    def get_tariff_code(self):
        groups = self.groups.all()
        if not groups:
            return None
        else:
            group = groups[0]
        return group.name

    def get_tariff(self):
        group = self.groups.first()
        return group

    def get_family_users(self):
        common_family= Family.objects.filter(user_id=self.pk).first()
        if common_family:
            return User.objects.filter(id__in=Family.objects.values_list('user_id',flat=True).\
                                       filter(common_id=common_family.common_id)).exclude(id=self.id)

    def get_family_all_users(self):
        common_family= Family.objects.filter(user_id=self.pk).first()
        if common_family:
            return User.objects.filter(id__in=Family.objects.values_list('user_id',flat=True).\
                                       filter(common_id=common_family.common_id))
        else:
            return []

    def get_tariff_name(self):
        group_name = self.get_tariff_code()
        if not group_name:
            return None

        russian_labels = {
            'base': 'Базовый',
            'food': 'Питание',
            'food_plus_sport': 'Питание + спорт',
            'food_plus_specialist': 'Питание + специалисты',
            'pregnancy': 'Беременность',
            'all_inclusive': 'Все включено',
            'family': 'Семейный',
            'children': 'Детский',
        }
        return russian_labels.get(group_name)

    def is_training_access(self):
        trainings = ['food_plus_sport', 'pregnancy', 'all_inclusive','consultants','family']
        group_name = self.get_tariff_code()
        if group_name in trainings:
            return True
        return False

    def is_articles_access(self):
        tariffes = ['food_plus_specialist', 'pregnancy', 'all_inclusive']
        group_name = self.get_tariff_code()
        if group_name in tariffes:
            return True
        return False

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
