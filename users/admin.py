from django.contrib import admin

from .models import User, Lifestyle, Family

class FamilyAdmin(admin.ModelAdmin):
    list_filter = ('common_id',)
    list_display = ('user','common_id', 'type')
    search_fields = ('user',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "user":
            kwargs["queryset"] = User.family_objects.all()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'knowledge', 'last_login')
    search_fields = ['email','first_name','last_name']
    list_filter = ['groups__name','knowledge', 'goal', 'lifestyle']


admin.site.register(Lifestyle)
admin.site.register(Family, FamilyAdmin)
admin.site.register(User, UserAdmin)
