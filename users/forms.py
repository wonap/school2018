from django import forms
from django.core.exceptions import ValidationError
from django.conf import settings
from .models import User
from django.forms import Select
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from common.models import KnowledgeLevel


class UserFamilyEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'avatar',
            'phone',
            'instagram',
            'gender',
            'height',
            'weight',
            'age',
            'lifestyle',
            'goal',
        )
        widgets = {
              'lifestyle': Select(attrs={'class': 'form-control'}),
          }

    def clean_avatar(self):
        avatar = self.cleaned_data.get('avatar', False)
        if avatar:
            if avatar.size > settings.MAX_UPLOAD_SIZE:
                raise ValidationError('Аватарка больше 4 мегабайтов')
            return avatar


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name',
                  'last_name',
                  'avatar',
                  'phone',
                  'instagram',
                  'gender',
                  'height',
                  'weight',
                  'age',
                  'lifestyle',
                  'goal',
                  )
        widgets = {
              'lifestyle': Select(attrs={'class': 'form-control'}),
          }

    def clean_avatar(self):
        avatar = self.cleaned_data.get('avatar', False)
        if avatar:
            if avatar.size > settings.MAX_UPLOAD_SIZE:
                raise ValidationError('Аватарка больше 4 мегабайтов')
            return avatar

RATES_CHOICES = (
    ('base', 'Базовый 3000₽'),
    ('food', 'Питание 3700₽'),
    ('children', 'Детский 4100₽'),
    ('food_plus_sport', 'Питание и спорт 4600₽'),
    ('food_plus_specialist', 'Питание и специалисты 4900₽'),
    ('pregnancy', 'Беременность 5000₽'),
    ('all_inclusive', 'Все включено для взрослых 6000₽'),
    ('family', 'Семейный 7000₽'),
)


class UserRegistrationForm(forms.Form):
    first_name = forms.CharField(label="Имя",max_length=100)
    last_name = forms.CharField(label="Фамилия", max_length=100)
    email = forms.EmailField(label="E-mail")
    password1 = forms.CharField(
        label="Пароль",
        strip=False,
        widget=forms.PasswordInput,
    )
    password2 = forms.CharField(
        label="Повторите пароль",
        widget=forms.PasswordInput,
        strip=False,
        help_text="Enter the same password as before, for verification.",
    )
    knowledge = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), queryset=KnowledgeLevel.objects.all())
    rate = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=RATES_CHOICES)
    promocod = forms.CharField(label="Промокод",max_length=100, required=False)

    def clean_email(self):
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError("Пользователь с таким email уже зарегистрирован в системе.")
        return self.cleaned_data['email']

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Ваши пароли не совпадают")
        # self.instance.username = self.cleaned_data.get('username')
        # password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2

    # def save(self, commit=True):
    #     user = super().save(commit=False)
    #     user.set_password(self.cleaned_data["password1"])
    #     if commit:
    #         user.save()
    #     return user


class ProlongForm(forms.Form):
    knowledge = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), queryset=KnowledgeLevel.objects.all())
    rate = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=RATES_CHOICES)
    promocod = forms.CharField(label="Промокод",max_length=100, required=False)


class AddUserInGroupForm(forms.Form):
    first_name = forms.CharField(label="Имя",max_length=100)
    last_name = forms.CharField(label="Фамилия", max_length=100)