from django.contrib import admin
from .models import PaymentCondition,PaymentTinkofCondition


class PaymentConditionAdmin(admin.ModelAdmin):
    list_display = ('user', 'tariff', 'payd', 'promocode','date_created', 'id')
    search_fields = ['user__email', 'id']
    list_filter = ['tariff','payd']

admin.site.register(PaymentCondition, PaymentConditionAdmin)



class PaymentTinkofConditionAdmin(admin.ModelAdmin):
    list_display = ('user', 'tariff', 'payd', 'date_created', 'payment_id')
    search_fields = ['user__email', 'payment_id']
    list_filter = ['tariff','payd']
admin.site.register(PaymentTinkofCondition, PaymentTinkofConditionAdmin)
