from django.db import models
from django.utils import timezone

from users.models import User
from common.models import Tariff


class PaymentCondition(models.Model):
    user = models.ForeignKey(User)
    tariff = models.ForeignKey(Tariff)
    payd = models.BooleanField(default=False)
    promocode = models.CharField(max_length=100, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    sum_payd = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True)

    def __str__(self):
        return str(self.user.pk)

    class Meta:
        verbose_name = 'Данные для оплаты'
        verbose_name_plural = 'Данные для оплаты'


class PaymentTinkofCondition(models.Model):
    payment_id = models.CharField(max_length=255)
    date_created = models.DateTimeField(default=timezone.now)
    sum_payd = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True)
    user = models.ForeignKey(User)
    tariff = models.ForeignKey(Tariff)
    payment_url = models.CharField(max_length=255)
    payd = models.BooleanField(default=False)
    payment_condition = models.ForeignKey(PaymentCondition, null=True, blank=True)

    def __str__(self):
        return self.payment_id

    class Meta:
        verbose_name = 'Данные для оплаты(Тинкофф)'
        verbose_name_plural = 'Данные для оплаты(Тинкофф)'
