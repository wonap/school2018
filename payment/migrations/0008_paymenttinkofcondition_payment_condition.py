# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-12-25 03:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0007_auto_20171225_0512'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenttinkofcondition',
            name='payment_condition',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='payment.PaymentCondition'),
        ),
    ]
