from django.conf.urls import url
from django.views.generic.base import TemplateView

from . import views


app_name = 'payment'
urlpatterns = [
    url(r'^$', views.TestView.as_view(), name='test'),
    url(r'^pay/(?P<pk>[0-9]+)/$', views.PayView.as_view(), name='pay-tariff'),
    url(r'^prolong/$', views.ProlongView.as_view(), name='prolong'),
    # url(r'^yandex/checktest$', views.CheckOrderView.as_view(), name='check-order'),
    # url(r'^yandex/avisotest$', views.PaymentAvisoView.as_view(), name='payment-aviso'),
    # url(r'^successtest$', TemplateView.as_view(template_name='message.html'), {
    #     'message': 'Оплата прошла успешно',
    # }, name='payment-success'),
    # url(r'^failtest$', TemplateView.as_view(template_name='message.html'), {
    #     'message': 'Произошла ошибка при оплате',
    # }, name='payment-fail'),
    url(r'^yandex/check$', views.CheckOrderView.as_view(), name='check-order'),
    url(r'^yandex/aviso$', views.PaymentAvisoView.as_view(), name='payment-aviso'),
    url(r'^success$', TemplateView.as_view(template_name='message.html'), {
        'message': 'Оплата прошла успешно',
    }, name='payment-success'),
    url(r'^fail$', TemplateView.as_view(template_name='message.html'), {
        'message': 'Произошла ошибка при оплате',
    }, name='payment-fail'),
    url(r'^tinkof/getlist/$',views.TinkofListView.as_view(), name='tinkof-getlist'),
    url(r'^tinkof/oplata/(?P<pk>[0-9]+)$',views.TinkofPayView.as_view(), name='tinkof-oplata'),
    url(r'^tinkof/success/$',views.TinkofSuccessView.as_view(), name='tinkof-success'),
    url(r'^tinkof/fail/$',views.TinkofFailView.as_view(), name='tinkof-fail'),
    url(r'^tinkof/prolong/$', views.TinkofProlongView.as_view(), name='tinkof-prolong'),
]
