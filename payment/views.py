import logging
import requests
from hashlib import md5,sha256

from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect, Http404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse_lazy
from django.core.mail import send_mail, BadHeaderError

from users.forms import UserRegistrationForm, ProlongForm
from users.models import User
from common.models import Tariff, MainSettings
from .forms import CheckOrderForm, PaymentAvisoForm
from .models import PaymentCondition,PaymentTinkofCondition

from django.template import Context, Template
from django.template.loader import get_template


class TestView(View):
    template_name = 'payment/test.html'

    def get(self, request, *args, **kwargs):
        form = UserRegistrationForm()
        ctx = dict()
        ctx['form'] = form
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = User()
            user.first_name = form.cleaned_data["first_name"]
            user.last_name = form.cleaned_data["last_name"]
            user.email = form.cleaned_data["email"]
            user.username = form.cleaned_data["email"]
            user.knowledge = form.cleaned_data["knowledge"]
            password = form.cleaned_data["password1"]
            user.set_password(password)
            user.is_active = True
            user.save()
            promocod = form.cleaned_data["promocod"]
            rate = form.cleaned_data["rate"]
            g = Group.objects.filter(name=rate).first()
            if g:
                g.user_set.add(user)

            pc = PaymentCondition()
            pc.user = user
            tariff = Tariff.objects.filter(code=rate).first()
            pc.tariff = tariff
            pc.promocode = promocod
            pc.save()

            send_mail('Вы зарегистрировались на school.natalyazubareva.com',
                      get_template('email.html').render(
                          Context({
                              'username': user.email,
                              'password': password
                          })
                      ),
                      settings.EMAIL_HOST_USER, ['' + user.email + ''])

            return redirect(reverse_lazy('payment:tinkof-oplata', kwargs={
                'pk': user.pk
            }))
        else:
            ctx = dict()
            ctx['form'] = form
            return render(request, self.template_name, ctx)


class ProlongView(View):
    template_name = 'payment/prolong.html'

    def get(self, request, *args, **kwargs):
        form = ProlongForm()
        ctx = dict()
        ctx['form'] = form
        user = request.user
        pc_yes = PaymentCondition.objects.filter(user=user).first()
        # if pc_yes:
        #     return redirect(reverse_lazy('payment:pay-tariff',kwargs={
        #         'pk': user.pk
        #     }))
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        form = ProlongForm(request.POST)
        user = request.user
        if form.is_valid():
            knowledge = form.cleaned_data["knowledge"]
            user.knowledge = knowledge
            user.save()
            promocod = form.cleaned_data["promocod"]
            rate = form.cleaned_data["rate"]
            g = Group.objects.filter(name=rate).first()
            if g:
                user_group = User.groups.through.objects.get(user=user)
                user_group.group = g
                user_group.save()

            pc_yes = PaymentCondition.objects.filter(user=user).first()
            tariff = Tariff.objects.filter(code=rate).first()
            if not pc_yes:
                pc = PaymentCondition()
                pc.user = user
                pc.tariff = tariff
                pc.promocode = promocod
                pc.save()
            else:
                pc_yes.tariff = tariff
                pc_yes.promocode = promocod
                pc_yes.save()
            return redirect(reverse_lazy('payment:pay-tariff', kwargs={
                'pk': user.pk
            }))
        else:
            ctx = dict()
            ctx['form'] = form
            return render(request, self.template_name, ctx)


class PayView(View):
    template_view = 'payment/pay.html'

    def get(self, request, *args, **kwargs):
        user = User.objects.filter(pk=kwargs.get('pk')).first()
        pc = PaymentCondition.objects.filter(user=user, payd=False).first()
        if not pc or not user:
            raise Http404

        sum = pc.tariff.cost
        if pc.promocode:
            promocode_info = MainSettings.objects.filter(code=pc.promocode).first()
            if promocode_info:
                sum = pc.tariff.cost - (pc.tariff.cost / 100 * int(promocode_info.value))

        ctx = dict()
        ctx['customer_id'] = user.pk
        ctx['order_number'] = pc.pk
        ctx['sum'] = int(sum)
        ctx['tariff'] = pc.tariff
        return render(request, self.template_view, ctx)


class TinkofPayView(View):
    template_name = 'payment/tinkof_pay_view.html'

    def get(self, request, *args,**kwargs):
        ctx = {}
        user = User.objects.filter(pk=kwargs.get('pk')).first()
        payment = PaymentCondition.objects.filter(user=user,payd=False).first()
        if not payment or not user:
            raise Http404
        sum = payment.tariff.cost
        if payment.promocode:
            promocode_info = MainSettings.objects.filter(code=payment.promocode).first()
            if promocode_info:
                sum = payment.tariff.cost - (payment.tariff.cost / 100 * int(promocode_info.value))

        payment_tinkoff = PaymentTinkofCondition.objects.filter(user=user).first()
        if not payment_tinkoff:
            data = {
                "TerminalKey": settings.TINKOF_TERMINAL_KEY,
                "Amount": int(sum * 100),
                "OrderId": payment.pk,
            }
            r = requests.post('https://securepay.tinkoff.ru/v2/Init', json=data)
            result = r.json()
            print(result)
            ctx['result'] = result
            payment_id = result.get('PaymentId')

            link_pay = result.get('PaymentURL')
            if not payment_id:
                raise Http404
            p_tinkoff = PaymentTinkofCondition()
            p_tinkoff.user = user
            p_tinkoff.payment_url = link_pay
            p_tinkoff.payment_id = payment_id
            p_tinkoff.sum_payd = sum
            p_tinkoff.tariff = payment.tariff
            p_tinkoff.payment_condition = payment
            p_tinkoff.save()
        else:
            link_pay = payment_tinkoff.payment_url
            payment_id = payment_tinkoff.payment_id

        ctx['payment_id'] = payment_id
        ctx['link_pay'] = link_pay
        ctx['customer_id'] = user.pk
        ctx['order_number'] = payment.pk
        ctx['sum'] = int(sum)
        ctx['tariff'] = payment.tariff
        return render(request,self.template_name,ctx)


class CheckMd5(object):
    def make_md5(self, cd):
        params = [cd['action'],
                  str(cd['orderSumAmount']),
                  str(cd['orderSumCurrencyPaycash']),
                  str(cd['orderSumBankPaycash']),
                  str(cd['shopId']),
                  str(cd['invoiceId']),
                  str(cd['customerNumber']),
                  settings.YANDEX_MONEY_PASSWORD]

        s = str(';'.join(params)).encode('utf-8')
        return md5(s).hexdigest().upper()


class CheckOrderView(CheckMd5, View):
    form_class = CheckOrderForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        logging.debug('TmpCheckOrderView')
        form = self.form_class(request.POST)
        print('===========checkorder==========')

        if form.is_valid():
            res = """<?xml version="1.0" encoding="utf-8"?>
                <checkOrderResponse
                    performedDatetime="{date}"
                    code="0"
                    invoiceId="{invoice_id}"
                    shopId="{shop_id}"/>
            """.format(
                date=timezone.now().isoformat(),
                invoice_id=form.cleaned_data.get('invoiceId'),
                shop_id=form.cleaned_data.get('shopId'),
            )

            return HttpResponse(res, content_type='application/xml')
        else:
            res = """<?xml version="1.0" encoding="utf-8"?>
                <checkOrderResponse
                    performedDatetime="{date}"
                    code="1"
                    invoiceId="{invoice_id}"
                    shopId="{shop_id}"
                    message="Check order, validation error"
                    techMessage="Check order, validation error"/>
            """.format(
                date=timezone.now().isoformat(),
                invoice_id=form.cleaned_data.get('invoiceId'),
                shop_id=form.cleaned_data.get('shopId'),
            )

            return HttpResponse(res, content_type='application/xml')


class PaymentAvisoView(CheckMd5, View):
    form_class = PaymentAvisoForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        print('===========payment==========')

        data = {k: str(v) for k, v in form.data.items()}

        if form.is_valid():
            cd = form.cleaned_data
            user_id = form.data.get('customerNumber')
            order_number = form.data.get('orderNumber')
            order_sum_amount = form.data.get('orderSumAmount')

            pc = PaymentCondition.objects.filter(id=order_number).first()
            if pc:
                pc.payd = True
                pc.sum_payd = float(order_sum_amount)
                pc.save()

            user = User.objects.filter(id=user_id).first()
            user.is_active = True
            user.save()

            res = """<?xml version="1.0" encoding="utf-8"?>
                <paymentAvisoResponse
                    performedDatetime="{date}"
                    code="0"
                    invoiceId="{invoice_id}"
                    shopId="{shop_id}"/>
            """.format(
                date=timezone.now().isoformat(),
                invoice_id=form.cleaned_data.get('invoiceId'),
                shop_id=form.cleaned_data.get('shopId'),
            )

            return HttpResponse(res, content_type='application/xml')
        else:
            res = """<?xml version="1.0" encoding="utf-8"?>
                <paymentAvisoResponse
                    performedDatetime="{date}"
                    code="1"
                    message="Payment aviso, validation error"
                    techMessage="Payment aviso, validation error"/>
            """.format(date=timezone.now().isoformat())

            return HttpResponse(res, content_type='application/xml')
            # return HttpResponse('<pre>{msg}</pre>'.format(msg=pformat(form.errors))) # Debug


class TinkofListView(View):

    def get_token(self, data=None):
        token = ''
        data["Password"] = 'zket6xzmpfk43g75'
        for key in sorted(data.keys()):
            token += str(data.get(key))
        token = token.encode('utf-8')
        return sha256(token).hexdigest()

    def get(self, request,*args, **kwargs):
        payment_id = 7519904
        url = "https://securepay.tinkoff.ru/v2/GetState"
        input = {"TerminalKey": settings.TINKOF_TERMINAL_KEY,
                 "PaymentId":payment_id}
        token = self.get_token(input)
        print(token)
        r = requests.post(url, json={"TerminalKey": settings.TINKOF_TERMINAL_KEY,
                                     "PaymentId":payment_id,
                                     "Token": token})
        print(r.text)
        return HttpResponse(r.text)
        # return JsonResponse(data)


class TinkofFailView(View):

    template_name = 'payment/tinkof_fail.html'
    def get(self, request, *args, **kwargs):
        ctx = {}
        payment_id = request.GET.get('PaymentId')
        payment_tinkoff = PaymentTinkofCondition.objects.filter(payment_id=payment_id).delete()
        return render(request, self.template_name,ctx)


class TinkofSuccessView(View):
    template_name = 'payment/tinkof_success.html'
    def get_token(self, data=None):
        token = ''
        data["Password"] = settings.TINKOF_PASSWORD
        for key in sorted(data.keys()):
            token += str(data.get(key))
        token = token.encode('utf-8')
        return sha256(token).hexdigest()

    def get(self, request, *args, **kwargs):
        payment_id = request.GET.get('PaymentId')
        url = "https://securepay.tinkoff.ru/v2/GetState"
        input = {"TerminalKey": settings.TINKOF_TERMINAL_KEY,
                 "PaymentId":payment_id}
        token = self.get_token(input)
        r = requests.post(url, json={"TerminalKey": settings.TINKOF_TERMINAL_KEY,
                                     "PaymentId":payment_id,
                                     "Token": token})
        data = r.json()
        status = data.get('Status')
        print(data)
        payment_tinkoff = PaymentTinkofCondition.objects.filter(payment_id=payment_id).first()
        if status == 'CONFIRMED':
            payment_tinkoff.payd = True
            payment_tinkoff.save()
            payment_condition = PaymentCondition.objects.filter(id=payment_tinkoff.payment_condition_id).first()
            if payment_condition:
                payment_condition.payd = True
                payment_condition.save()
        ctx = {}
        return render(request, self.template_name,ctx)


class TinkofProlongView(View):
    template_name = 'payment/prolong.html'

    def get(self, request, *args, **kwargs):
        form = ProlongForm()
        ctx = dict()
        ctx['form'] = form
        user = request.user
        pc_yes = PaymentTinkofCondition.objects.filter(user=user).first()
        # if pc_yes:
        #     return redirect(reverse_lazy('payment:pay-tariff',kwargs={
        #         'pk': user.pk
        #     }))
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        form = ProlongForm(request.POST)
        user = request.user
        if form.is_valid():
            knowledge = form.cleaned_data["knowledge"]
            user.knowledge = knowledge
            user.save()
            promocod = form.cleaned_data["promocod"]
            rate = form.cleaned_data["rate"]
            g = Group.objects.filter(name=rate).first()
            if g:
                user_group = User.groups.through.objects.get(user=user)
                user_group.group = g
                user_group.save()

            pc_yes = PaymentCondition.objects.filter(user=user).first()
            tariff = Tariff.objects.filter(code=rate).first()
            if not pc_yes:
                pc = PaymentCondition()
                pc.user = user
                pc.tariff = tariff
                pc.promocode = promocod
                pc.save()
            else:
                pc_yes.tariff = tariff
                pc_yes.promocode = promocod
                pc_yes.save()
            PaymentTinkofCondition.objects.filter(user=user).delete()
            return redirect(reverse_lazy('payment:tinkof-oplata', kwargs={
                'pk': user.pk
            }))
        else:
            ctx = dict()
            ctx['form'] = form
            return render(request, self.template_name, ctx)