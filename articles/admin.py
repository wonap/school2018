from django.contrib import admin
from users.models import User
from .models import Articles, Comment

def make_published(modeladmin, request, queryset):
    queryset.update(is_show='False')
make_published.short_description = "Отметить как неопубликованные"


class ArticlesAdmin(admin.ModelAdmin):
    list_display = ('title','author','knowledge','created_date','is_show')
    list_filter = ('knowledge','published_date','author',)
    actions = [make_published]


    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "author":
            kwargs["queryset"] = User.consultants_objects.all()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Articles, ArticlesAdmin)
admin.site.register(Comment)
