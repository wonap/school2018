from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView

from common.mixins import PayCheck
from .models import Articles
from .forms import CommentForm


class ArticlesListView(LoginRequiredMixin, PayCheck, ListView):
    model = Articles
    template_name = 'articles/articles_list.html'
    context_object_name = 'articles'

    def get_queryset(self):
        knowledge = self.request.user.knowledge
        custom_kwargs = dict()
        custom_kwargs['knowledge'] = knowledge
        custom_kwargs['published_date__lte'] = timezone.now()

        if self.request.user.is_food_user():
            custom_kwargs['author__type__in'] = ['zubareva']
        elif self.request.user.is_food_plus_sport_user():
            custom_kwargs['author__type__in'] = ['zubareva','coach','neurologist']
        elif self.request.user.is_food_plus_specialist_user():
            custom_kwargs['author__type__in'] = ['zubareva','neurologist','gastro','psychologist','gv','gynecologist','endoc']
        elif self.request.user.is_children_user():
            custom_kwargs['author__type__in'] = ['zubareva', 'pediatrician', 'gv']
        elif self.request.user.is_family_user():
            custom_kwargs['knowledge'] = knowledge
        elif self.request.user.is_articles_access():
            pass
        else:
            return Articles.objects.filter(is_show=True, published_date__lte=timezone.now())\
                                .order_by('-published_date')

        return Articles.objects.filter(is_show=True, **custom_kwargs).order_by('-published_date')


class ArticlesDetailView(LoginRequiredMixin,DetailView):
    model = Articles
    template_name = 'articles/articles_detail.html'

    def dispatch(self, request, *args, **kwargs):
        # article = self.get_object()
        # current_user = self.request.user
        # if not current_user.is_consultant_manager():
        #     if article.knowledge != current_user.knowledge:
        #         raise Http404
            # if current_user.is_food_user() and article.author.type in ['zubareva']:
            #     raise Http404
            # if current_user.is_food_plus_sport_user() and article.author.type in ['zubareva','coach']:
            #     raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['form'] = CommentForm()
        return ctx


def add_comment(request):
    form = CommentForm(request.POST)
    if form.is_valid():
        comment = form.save()
        to_redirect = reverse_lazy('articles:detail', kwargs={'pk': comment.article.pk})
        return redirect(to_redirect)
