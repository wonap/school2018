from django.conf.urls import url
from . import views

app_name = 'articles'
urlpatterns = [
    url(r'^$', views.ArticlesListView.as_view(), name='list'),
    url(r'^articles/(?P<pk>[0-9]+)/$', views.ArticlesDetailView.as_view(), name='detail'),
    url(r'^add_comment/$', views.add_comment, name='add-comment'),
]
