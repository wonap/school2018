from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField
from common.models import KnowledgeLevel
from mptt.models import TreeForeignKey, MPTTModel
from users.models import User


class Articles(models.Model):
    author = models.ForeignKey('users.User')
    title = models.CharField(max_length=200)
    img = models.ImageField(upload_to='articles')
    text = RichTextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    knowledge = models.ForeignKey(KnowledgeLevel, related_name='articles',
                                  null=True, blank=True)
    is_show = models.BooleanField(default=False)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-published_date',)
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'


class Comment(MPTTModel):
    article = models.ForeignKey(Articles, related_name='article_comments')
    author = models.ForeignKey(User)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=True)
    parent = TreeForeignKey('self', blank=True, null=True,
                            related_name='children', db_index=True)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
