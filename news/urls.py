from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

app_name = 'news'
urlpatterns = [
    # url(r'^$', views.news_list, name='list'),
    url(r'^$', views.NewsListView.as_view(), name='list'),
    #url(r'^news/(?P<pk>[0-9]+)/$', views.news_detail, name='detail'),
    url(r'^news/(?P<pk>[0-9]+)/$', views.NewsDetailView.as_view(), name='detail'),
    url(r'^not/$', views.NotcommentListView.as_view(), name='not-list'),
    #url(r'^news/(?P<pk>\d+)/comment/$', views.add_comment_to_news, name='add_comment_to_news'),
    url(r'^add_comment/$', views.add_comment, name='add-comment'),
    url(r'^news/comments/$', views.CommentListView.as_view(), name='list-comment'),
    url(r'^news/instructions/$', TemplateView.as_view(template_name="instructions.html"), name='instructions'),
    url(r'^news/faqrecipe/$', TemplateView.as_view(template_name="faqrecipe.html"), name='faqrecipe'),

    url(r'^news/mycomments/$', views.MycommentListView.as_view(), name='mycomments-list'),
    url(r'^news/mycomments/(?P<pk>[0-9]+)/$', views.MycommentDetailView.as_view(), name='mycomments-detail'),
    url(r'^add_mycomment/$', views.add_mycomment, name='add-mycomment'),
]
