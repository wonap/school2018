# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-03-22 20:22
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('news', '0010_news_video'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsNewComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='new_author_comments', to=settings.AUTH_USER_MODEL)),
                ('comment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='new_comments', to='news.Comment')),
                ('parent', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='new_parent_comment', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
