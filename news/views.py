from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView
from django.views.generic.base import View

from common.mixins import PayCheck
from .models import News, Comment, NewsNewComment
from .forms import CommentForm
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

class NewsListView(LoginRequiredMixin, PayCheck,ListView):
    model = News
    template_name = 'news/news_list.html'
    context_object_name = 'news'

    def get_queryset(self):
        return News.objects.filter(is_show=True, published_date__lte=timezone.now()).order_by('-published_date')


class NewsDetailView(LoginRequiredMixin, DetailView):
    model = News


    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        comments_list = self.get_object().comments.filter(level=0).order_by('-created_date')

        paginator = Paginator(comments_list, 30)
        page = self.request.GET.get('page')
        try:
            comments = paginator.page(page)
        except PageNotAnInteger:
            comments = paginator.page(1)
        except EmptyPage:
            comments.page(paginator.num_pages)

        ctx['form'] = CommentForm()
        ctx['comments'] = comments
        return ctx

    template_name = 'news/news_detail.html'


def add_comment(request):
    form = CommentForm(request.POST)
    if form.is_valid():
        comment = form.save()

        if comment.parent is None:
            print('no')
            new_comment = ''
        else:
            new_comment = NewsNewComment()
            new_comment.comment = comment
            new_comment.author = comment.author
            new_comment.parent = comment.parent.author
            new_comment.save()

        to_redirect = reverse_lazy('news:detail', kwargs={'pk': comment.news.pk})
        return redirect(to_redirect)


class AddCommentView(LoginRequiredMixin,View):

    def post(self, request, *args, **kwargs):
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save()
            to_redirect = reverse_lazy('news:detail', kwargs={'pk': comment.news.pk})
            return redirect(to_redirect)



class CommentListView(LoginRequiredMixin, PayCheck,ListView):
    model = Comment
    template_name = 'news/comments_list.html'
    context_object_name = 'comments'

    def get_queryset(self):
        return Comment.objects.filter(created_date__lte=timezone.now(),author=self.request.user).order_by('-created_date')[0:18]



class MycommentListView(LoginRequiredMixin, PayCheck,ListView):
    model = News
    template_name = 'comments/comments_list.html'
    context_object_name = 'comments'

    def get_queryset(self):
        return News.objects.filter(is_show=True, published_date__lte=timezone.now()).order_by('-published_date')


class MycommentDetailView(LoginRequiredMixin, DetailView):
    model = News


    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['form'] = CommentForm()
        ctx['comments'] = self.get_object().comments.filter(author=self.request.user).order_by('-created_date')
        return ctx

    template_name = 'comments/comments_detail.html'


def add_mycomment(request):
    form = CommentForm(request.POST)
    if form.is_valid():
        comment = form.save()

        if comment.parent is None:
            print('no')
            new_comment = ''
        else:
            new_comment = NewsNewComment()
            new_comment.comment = comment
            new_comment.author = comment.author
            new_comment.parent = comment.parent.author
            new_comment.save()

        to_redirect = reverse_lazy('news:mycomments-detail', kwargs={'pk': comment.news.pk})
        return redirect(to_redirect)



class NotcommentListView(LoginRequiredMixin,ListView):
    model = Comment
    template_name = 'notcomment.html'

    def get(self, request, *args, **kwargs):
        ctx = {}

        #notcomment = Comment.objects.filter(created_date__lte=timezone.now(),author=self.request.user).order_by('-created_date')[0:18]
        notcomment = Comment.objects.filter(parent__author=self.request.user).order_by('-created_date')[:25]

        NewsNewComment.objects.filter(parent=self.request.user).delete()
        comment_counts = NewsNewComment.objects.filter(parent=self.request.user).count()



        ctx['notcomment'] = notcomment
        ctx['comment_counts'] = comment_counts
        return render(request, self.template_name,ctx)
