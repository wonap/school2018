from django.contrib import admin
from users.models import User
from .models import News, Comment, NewsNewComment

def make_published(modeladmin, request, queryset):
    queryset.update(is_show='False')
make_published.short_description = "Отметить как неопубликованные"


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title','author','created_date','is_show')
    list_filter = ('published_date','author',)
    actions = [make_published]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "author":
            kwargs["queryset"] = User.consultants_objects.all()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

class CommentAdmin(admin.ModelAdmin):
    list_display = ('text', 'author', 'created_date', 'news')
    search_fields = ['text', 'author__email']
    list_filter = ('news','created_date')
    ordering = ('-created_date',)


admin.site.register(News, NewsAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(NewsNewComment)