from django.db import models
from django.utils import timezone
from mptt.models import TreeForeignKey, MPTTModel
from ckeditor.fields import RichTextField
from users.models import User


class News(models.Model):
    author = models.ForeignKey('users.User')
    title = models.CharField(max_length=200)
    video = models.TextField(blank=True, null=True, default=None)
    img = models.ImageField(upload_to='news')
    text = RichTextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    is_show = models.BooleanField(default=False)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'


class Comment(MPTTModel):
    news = models.ForeignKey('news.News', related_name='comments')
    author = models.ForeignKey(User, related_name='author')
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=True)
    # parent = models.ForeignKey('self', null=True, blank=True)
    parent = TreeForeignKey('self', blank=True, null=True,
                            related_name='children', db_index=True)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Комментария'
        verbose_name_plural = 'Комментарии'



class NewsNewComment(models.Model):
    author = models.ForeignKey(User, related_name='new_author_comments')
    parent = models.ForeignKey(User,related_name='new_parent_comment')
    comment = models.ForeignKey(Comment, related_name='new_comments')


    def __str__(self):
        return str(self.id)
