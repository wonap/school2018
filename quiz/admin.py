from django.contrib import admin

from .models import Quiz, Question, Answer, Choice, Result


class AnswerInline(admin.TabularInline):
    model = Answer


class QuizAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_filter = ('knowledge',)

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('content',)
    list_filter = ('quiz',)
    inlines = [AnswerInline]


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('content',)
    list_filter = ('question',)


class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('answer',)


class ResultAdmin(admin.ModelAdmin):
    list_display = ('user',)


admin.site.register(Quiz, QuizAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Result, ResultAdmin)
