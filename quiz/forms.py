from django import forms
from django.core.exceptions import ValidationError

from quiz.models import Choice


class ChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ('user', 'question','quiz', 'answer')

    def clean(self):
        cleaned_data = self.cleaned_data
        try:
            Choice.objects.get(user=cleaned_data['user'], question=cleaned_data['question'])
        except Choice.DoesNotExist:
            pass
        else:
            raise ValidationError('Вы уже отвечали на этот вопрос')

        return cleaned_data
