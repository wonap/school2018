# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-14 19:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0004_auto_20170213_1634'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='quiz',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='choices_quiz', to='quiz.Quiz'),
            preserve_default=False,
        ),
    ]
