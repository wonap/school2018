from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import View, ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from common.mixins import PayCheck

from quiz.forms import ChoiceForm
from quiz.models import Quiz, Question, Choice, Answer, Result


class QuizListView(LoginRequiredMixin, PayCheck,ListView):
    model = Quiz
    template_name = 'quiz/quiz_list.html'

    def get_queryset(self):
        knowledge = self.request.user.knowledge
        if knowledge:
            return Quiz.objects.filter(knowledge=knowledge).filter(pk__in=[6, 7, 8])
        else:
            return Quiz.objects.all()


class QuizDetailView(LoginRequiredMixin,DetailView):
    model = Quiz
    template_name = 'quiz/quiz_detail.html'


class QuizStartView(LoginRequiredMixin, View):
    login_url = '/login/'
    template_name = 'quiz/quiz_start.html'

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        ctx = dict()
        quiz = Quiz.objects.get(pk=pk)
        question_ids = Choice.objects.values_list('question_id', flat=True)\
            .filter(user=request.user, quiz=quiz)
        question = quiz.questions.exclude(id__in=question_ids).first()
        if not question and len(question_ids)>0:
            answer_ids = Choice.objects.values_list('answer').filter(user=request.user, quiz=quiz)
            results = Answer.objects.filter(id__in=answer_ids, correct=True).count()
            results = results * 5
            add_result_for_user = Result.objects.get_or_create(quiz=quiz,
                                                               user=request.user,
                                                               score=results)
            ctx['results'] = {'score':results}

        choice_form = ChoiceForm()

        ctx['question'] = question
        ctx['choice_form'] = choice_form
        ctx['quiz'] = quiz
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = dict()
        ctx['question'] = Question.objects.get(pk=request.POST.get('question'))
        choice_form = ChoiceForm(request.POST)
        if choice_form.is_valid:
            choice = choice_form.save()
            return redirect(reverse_lazy('quiz:start', kwargs={'pk': kwargs.get('pk')}))
        else:
            return render(request, self.template_name, ctx)


class QuizUserResult(LoginRequiredMixin, View):
    pass


class ResultsView(LoginRequiredMixin,View):
    template_name = 'quiz/quiz_results.html'

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        results = Result.objects.order_by('-score')
        ctx = dict()
        ctx['results'] = results
        return render(request, self.template_name, ctx)
