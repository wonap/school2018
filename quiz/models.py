from django.db import models

from common.models import Week, KnowledgeLevel
from users.models import User


class Quiz(models.Model):
    title = models.CharField(max_length=100, blank=False)
    description = models.TextField(blank=True)
    week = models.ForeignKey(Week)
    knowledge = models.ForeignKey(KnowledgeLevel, related_name='quizes',
                                  null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тест'
        verbose_name_plural = 'Тесты'

    def get_questions(self):
        return self.questions.all()

    def get_max_score(self):
        return self.get_questions().count()


class Question(models.Model):
    quiz = models.ForeignKey(Quiz, related_name='questions')
    content = models.TextField()
    explanation = models.TextField(blank=True)
    position = models.IntegerField(default=0)
    correct = models.BooleanField(blank=False, default=False)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'


class Answer(models.Model):
    question = models.ForeignKey(Question, related_name='answers')
    content = models.TextField()
    correct = models.BooleanField(blank=False, default=False)
    score = models.IntegerField(default=1)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'
        ordering = ('-id',)

class Choice(models.Model):
    question = models.ForeignKey(Question, related_name='choices')
    quiz = models.ForeignKey(Quiz, related_name='choices_quiz')
    answer = models.ForeignKey(Answer, related_name='choices_answer')
    user = models.ForeignKey(User)

    def __str__(self):
        return str(self.pk)

    class Meta:
        unique_together = ('user', 'question')
        verbose_name = 'Ответ пользователя'
        verbose_name_plural = 'Ответы пользователей'


class Result(models.Model):
    quiz = models.ForeignKey(Quiz, related_name='quiz_results')
    user = models.ForeignKey(User, related_name='user_results')
    score = models.IntegerField(default=0)

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Результат'
        verbose_name_plural = 'Результаты'
