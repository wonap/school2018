from django.conf.urls import url
from . import views

app_name = 'quiz'

urlpatterns = [
    url(r'^$', views.QuizListView.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', views.QuizDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/start/$', views.QuizStartView.as_view(), name='start'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
]
