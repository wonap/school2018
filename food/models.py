from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

from users.models import User


class MenuType(models.Model):
    name = models.CharField(max_length=100)
    code = models.SlugField(max_length=100, null=True, blank=True)
    is_show = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Вид меню'
        verbose_name_plural = 'Вид меню'


class Calory(models.Model):
    value = models.CharField(max_length=100)

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = 'Калории'
        verbose_name_plural = 'Калории'


class Recipe(models.Model):
    name = models.CharField(max_length=150)
    img = models.ImageField(upload_to='foods')
    description = RichTextField(blank=True)
    menu_type = models.ManyToManyField(MenuType)
    calories = models.ManyToManyField(Calory, null=True, blank=True)
    week = models.ForeignKey('common.Week', related_name='week_recipes', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Рецепт'
        verbose_name_plural = 'Рецепты'


class MenuPage(models.Model):
    name = models.CharField(max_length=255)
    content = RichTextField(blank=True)
    menu_type = models.ManyToManyField(MenuType)
    calories = models.ManyToManyField(Calory,null=True, blank=True)
    week = models.ForeignKey('common.Week', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Меню на неделю'
        verbose_name_plural = 'Меню на неделю'


class ProductPage(models.Model):
    name = models.CharField(max_length=255)
    content = RichTextField(blank=True)
    menu_type = models.ManyToManyField(MenuType)
    calories = models.ManyToManyField(Calory, null=True, blank=True)
    week = models.ForeignKey('common.Week', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Продукты на неделю'
        verbose_name_plural = 'Продукты на неделю'


class ChoiceMenuCalory(models.Model):
    calory = models.ForeignKey(Calory, null=True, blank=True)
    menu_type = models.ForeignKey(MenuType, null=True, blank=True)
    user = models.ForeignKey(User)

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = 'Выбор пользователя'
        verbose_name_plural = 'Выбор пользователя'


