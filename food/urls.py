from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

app_name = 'food'
urlpatterns = [
    # url(r'^$', views.FoodListView.as_view(), name='list'),
    url(r'^$', views.FoodMainListView.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', views.FoodDetailView.as_view(), name='detail'),
    url(r'^get_calories/$', views.get_calories, name='get_calories'),
    url(r'^goal/$', TemplateView.as_view(template_name="goal.html")),
    url(r'^lose/$', TemplateView.as_view(template_name="lose.html")),
]
