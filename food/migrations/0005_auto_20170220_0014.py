# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-19 21:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0002_knowledgelevel'),
        ('food', '0004_auto_20170219_2352'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('content', models.TextField(blank=True)),
                ('calories', models.ManyToManyField(to='food.Calory')),
                ('menu_type', models.ManyToManyField(to='food.MenuType')),
                ('week', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='common.Week')),
            ],
            options={
                'verbose_name': 'Продукты',
                'verbose_name_plural': 'Продукты',
            },
        ),
        migrations.RenameModel(
            old_name='FoodPage',
            new_name='MenuPage',
        ),
        migrations.AlterModelOptions(
            name='menupage',
            options={'verbose_name': 'Меню на неделю', 'verbose_name_plural': 'Меню на неделю'},
        ),
    ]
