from django.contrib import admin

from .models import (ChoiceMenuCalory,Recipe,
                     MenuType, Calory, MenuPage,
                     ProductPage, )


class MenuTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {"code": ("name",)}


class RecipeAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ('week', 'menu_type', 'calories')


class MenuPageAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ('week',)


class ChoiceMenuCaloryAdmin(admin.ModelAdmin):
    list_display = ('user',)
    list_filter = ('menu_type',)


admin.site.register(MenuType, MenuTypeAdmin)
admin.site.register(Calory)
admin.site.register(Recipe, RecipeAdmin)
admin.site.register(MenuPage, MenuPageAdmin)
admin.site.register(ProductPage)
admin.site.register(ChoiceMenuCalory, ChoiceMenuCaloryAdmin)
