import datetime
import json
from datetime import timedelta

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse
from django.shortcuts import render

from django.views.generic import TemplateView, View
from django.utils import timezone
from django.views.generic.detail import DetailView

from common.mixins import PayCheck
from food.models import Recipe, Calory, MenuType, MenuPage, ProductPage, ChoiceMenuCalory
from common.models import Week


class FoodListView(TemplateView):
    template_name = 'food/food_list.html'


class FoodMainListView(LoginRequiredMixin,PayCheck, View):
    template_name = 'food/food_main_list.html'
    # template_name = 'food/food_list.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        current_time = datetime.datetime.now()
        week = Week.objects.filter(begin__lte=current_time, end__gte=current_time).first()
        menu_type = request.GET.get('menu_type')
        calories = request.GET.get('calories')
        if menu_type:
            ch_menu,created = ChoiceMenuCalory.objects.get_or_create(user=request.user)
            ch_menu.menu_type = MenuType.objects.filter(id=menu_type).first()
            if calories:
                ch_menu.calory = Calory.objects.filter(value=calories).first()
            else:
                ch_menu.calory = None
            ch_menu.save()

        if not menu_type and not calories:
            choice = ChoiceMenuCalory.objects.filter(user=request.user).first()
            if choice:
                menu_type = choice.menu_type.pk
                if choice.calory:
                    calories = choice.calory.value

        custom_kwargs = {}
        custom_kwargs['week'] = week
        if menu_type:
            custom_kwargs['menu_type__in'] = [menu_type]
        if calories:
            custom_kwargs['calories__value__in'] = [calories]
        if request.user.is_pregnancy_user():
            recipes_list = Recipe.objects.filter(**custom_kwargs)
        else:
            recipes_list = Recipe.objects.filter(**custom_kwargs).exclude(menu_type__code='dlya-beremennyh')

        menu_page = MenuPage.objects.filter(**custom_kwargs).first()
        product_page = ProductPage.objects.filter(**custom_kwargs).first()
        #
        # if request.user.is_pregnancy_user():
        #     recipes_list = Recipe.objects.filter(week=week, menu_type__code='dlya-beremennyh')
        #     menu_page = MenuPage.objects.filter(week=week, menu_type__code='dlya-beremennyh').first()
        #     product_page = ProductPage.objects.filter(week=week, menu_type__code='dlya-beremennyh').first()

        paginator = Paginator(recipes_list, 21)
        page = request.GET.get('page')
        try:
            recipes = paginator.page(page)
        except PageNotAnInteger:
            recipes = paginator.page(1)
        except EmptyPage:
            recipes.page(paginator.num_pages)

        menu_types_id_list = [8, 11, 12]
        pregnancy_id_list = [11, 12, 15, 16]
        family_id_list = [8, 12, 15, 16]
        exclude_id_list = [8, 11, 12, 15, 16]

        if request.user.is_pregnancy_user():
            menu_types = MenuType.objects.exclude(id__in=pregnancy_id_list)
        elif request.user.is_base_user():
            menu_types = MenuType.objects.exclude(id__in=exclude_id_list)
        elif request.user.is_food_user():
            menu_types = MenuType.objects.exclude(id__in=exclude_id_list)
        elif request.user.is_food_plus_sport_user():
            menu_types = MenuType.objects.exclude(id__in=exclude_id_list)
        elif request.user.is_food_plus_specialist_user():
            menu_types = MenuType.objects.exclude(id__in=exclude_id_list)
        elif request.user.is_children_user():
            menu_types = MenuType.objects.filter(code='detskoe-menyu')
        elif request.user.is_family_user():
            menu_types = MenuType.objects.exclude(id__in=family_id_list)
        else:
            menu_types = MenuType.objects.exclude(id__in=menu_types_id_list)
        if menu_type:
            recipes_menu_list = Recipe.objects.filter(menu_type__in=[menu_type], week=week)
            calories_list = Calory.objects.filter(recipe__id__in=recipes_menu_list).distinct()
        else:
            recipes_menu_list = Recipe.objects.filter(menu_type__in=[1],week=week)
            calories_list = Calory.objects.filter(recipe__id__in=recipes_menu_list).distinct()

        current_menu_type = menu_type
        current_calories = calories
        ctx['current_menu_type'] = current_menu_type
        ctx['current_calories'] = current_calories
        ctx['recipes'] = recipes
        ctx['calories'] = calories_list
        ctx['menu_types'] = menu_types
        ctx['menu_page'] = menu_page
        ctx['product_page'] = product_page
        return render(request, self.template_name,ctx)


class FoodDetailView(LoginRequiredMixin,PayCheck,DetailView):
    model = Recipe
    template_name = 'food/food_detail.html'


def get_calories(request):
    current_time = datetime.datetime.now()
    week = Week.objects.filter(begin__lte=current_time, end__gte=current_time).first()
    menu_type = request.GET.get('menu_type')
    recipes_menu_list = Recipe.objects.filter(menu_type__in=[menu_type], week=week)
    calories_list = Calory.objects.filter(recipe__id__in=recipes_menu_list).distinct()
    print(len(calories_list))
    data = [{'id': c.id, 'val': c.value} for c in calories_list]
    return HttpResponse(json.dumps(data), content_type='application/json')
