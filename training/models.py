from django.db import models

from common.models import Week
from ckeditor.fields import RichTextField


class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Training(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    second_title = models.CharField(max_length=200,blank=True, null=True)
    description = RichTextField()
    img = models.ImageField(upload_to='training')
    video_url = models.TextField(blank=True, null=True)
    category = models.ForeignKey(Category, related_name='trainings')
    week = models.ForeignKey(Week, null=True, blank=True)
    position = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тренировка'
        verbose_name_plural = 'Тренировки'
