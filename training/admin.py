from django.contrib import admin
from .models import Training, Category


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class TrainingAdmin(admin.ModelAdmin):
    list_display = ('title', "position", "second_title", 'category', 'week')
    list_filter = ('category', 'week')
    search_fields = ('title',)

admin.site.register(Category, CategoryAdmin)
admin.site.register(Training, TrainingAdmin)