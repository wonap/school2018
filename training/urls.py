from django.conf.urls import url
from . import views

app_name = 'training'
urlpatterns = [
    url(r'^$', views.TrainingListView.as_view(), name='list'),
    url(r'^training/(?P<pk>[0-9]+)/$', views.TrainingDetailView.as_view(), name='detail'),
]
