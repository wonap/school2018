import datetime
from django.shortcuts import render
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import ListView, DetailView
from common.models import Week
from common.mixins import PayCheck
from .models import Training, Category


class TrainingListView(PermissionRequiredMixin, PayCheck,ListView):
    model = Training
    permission_required = 'training.can_view'
    raise_exception = True
    template_name = 'training/list.html'
    context_object_name = 'training_list'

    def get_queryset(self):
        current_time = datetime.datetime.now()
        week = Week.objects.filter(begin__lte=current_time, end__gte=current_time).first()
        if self.request.user.is_pregnancy_user():

            trainings = Training.objects.\
                filter(week=week, category__slug='dlya-beremennyh').order_by('position')
        else:
            category = self.request.GET.get('category')
            custom_kwargs = {}
            if category:
                custom_kwargs['category__slug'] = category
            custom_kwargs['week'] = week
            trainings = Training.objects.filter(**custom_kwargs)\
                .exclude(category__slug='dlya-beremennyh').order_by('position')
        return trainings

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.request.user.is_pregnancy_user():
            ctx['category_list'] = Category.objects.filter(slug='dlya-beremennyh')
        else:
            ctx['category_list'] = Category.objects.exclude(slug='dlya-beremennyh')
        return ctx


class TrainingDetailView(PermissionRequiredMixin, DetailView):
    model = Training
    permission_required = 'training.can_view'
    raise_exception = True
    template_name = 'training/training_detail.html'
