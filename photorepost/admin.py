from django.contrib import admin
from django.forms import TextInput, Textarea
from django.db import models
from .models import Photorepost


class PhotoRepostAdmin(admin.ModelAdmin):
    list_display= ('title','created_date','author_last_name','author_first_name','author_goal','tariff_name','image_tag_small','is_checked',)
    readonly_fields = ('image_tag',)
    list_filter = ('published_date','author__groups__name','is_checked',)
    search_fields = ['author__email','author__first_name','author__last_name',]
    fields = ('title','image_tag','img','text','type_food','treatment','water','breakfast','one_snack','lunch','second_snack','dinner','rating','comment','author','is_checked',)
    date_hierarchy = 'published_date'
    list_per_page = 25
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '100'})},
    }

    def author_last_name(self, obj):
        return obj.author.last_name

    def author_first_name(self, obj):
        return obj.author.first_name

    def author_goal(self, obj):
        return obj.author.get_goal_display()

    def author_goal(self, obj):
        return obj.author.get_goal_display()

    def tariff_name(self, obj):
        return obj.author.get_tariff_name()

admin.site.register(Photorepost, PhotoRepostAdmin)
