import datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.views.generic import ListView, DetailView, View, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, get_object_or_404, redirect, Http404
from django.views.generic.edit import CreateView, FormView
from django.db.models import Sum
from common.mixins import PayCheck

from users.models import User
from .models import Photorepost
from .forms import PhotorepostForm


class PhotorepostListView(LoginRequiredMixin, PayCheck,ListView):
    model = Photorepost
    template_name = 'photorepost/photorepost_list.html'
    context_object_name = 'photorepost'

    def get_queryset(self):
        family_users = self.request.user.get_family_all_users()
        if family_users:
            ids = [u.pk for u in family_users]
        else:
            ids = [self.request.user.pk]
        return Photorepost.objects.filter(published_date__lte=timezone.now(),author_id__in=ids).order_by('-published_date')


class PhotorepostDetailView(LoginRequiredMixin, DetailView):
    model = Photorepost
    template_name = 'photorepost/photorepost_detail.html'

    def dispatch(self, request, *args, **kwargs):
        post = self.get_object()
        family_users = self.request.user.get_family_all_users()
        if family_users:
            ids = [u.pk for u in family_users]
            if post.author.pk not in ids:
                raise Http404
        else:
            if post.author != self.request.user:
                raise Http404

        return super().dispatch(request, *args, **kwargs)



def photorepost_new(request):
    form = PhotorepostForm(request.POST)
    if form.is_valid():
        photorepost = form.save()
        to_redirect = reverse_lazy('photorepost:detail', kwargs={'pk': photorepost.pk})
        return redirect(to_redirect)


class PhotoRepostCreateView(CreateView):
    model = Photorepost
    form_class = PhotorepostForm
    template_name = 'photorepost/photorepost_create.html'

    def form_valid(self, form):
        # form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('photorepost:detail', kwargs={'pk': self.object.id})


class PhotoRepostUpdateView(LoginRequiredMixin, UpdateView):
    model = Photorepost
    form_class = PhotorepostForm
    template_name = "photorepost/photorepost_update.html"

    def dispatch(self, request, *args, **kwargs):
        post = Photorepost.objects.get(pk=kwargs["pk"])
        if post.author != self.request.user:
            raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('photorepost:detail', kwargs={'pk': self.object.id})


class PhotoRepostResultView(LoginRequiredMixin, View):
    template_name = 'photorepost/photorepost_result.html'

    def get(self, request, *args, **kwargs):
        results = Photorepost.objects.filter(published_date__gte=datetime.date(2018,3,24)).values('author__email', 'author__first_name', 'author__last_name').\
            annotate(score=Sum('rating')).order_by('-score')
        return render(request,self.template_name, {
            'results': results,
        })
