from django.db import models
from django.utils import timezone


class Photorepost(models.Model):
    author = models.ForeignKey('users.User', null=True, blank=True)
    title = models.CharField(max_length=200, blank=True, null=True, default=None)
    img = models.ImageField(upload_to='photorepost')

    YES, NO = 'Y', 'N'
    FOOD_CHOICES = (
        (YES, 'Да'),
        (NO, 'Нет'),
    )
    treatment = models.CharField('Лечебное питание:', max_length=1, choices=FOOD_CHOICES, default=NO, blank=True)

    type_food = models.CharField('Вид меню:', max_length=200, blank=True, null=True, default=None)
    water = models.CharField('Питьевой режим:', max_length=200, blank=True, null=True, default=None)
    breakfast = models.CharField('Завтрак:', max_length=200, blank=True, null=True, default=None)
    one_snack = models.CharField('1ый перекус:', max_length=200, blank=True, null=True, default=None)
    lunch = models.CharField('Обед:', max_length=200, blank=True, null=True, default=None)
    second_snack = models.CharField('2ой перекус:', max_length=200, blank=True, null=True, default=None)
    dinner = models.CharField('Ужин:', max_length=200, blank=True, null=True, default=None)

    text = models.TextField(blank=True, null=True, default=None)
    rating = models.IntegerField(default=0)
    comment = models.TextField(blank=True, null=True, default=None)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(default=timezone.now, blank=True, null=True)
    CHECKED, UNCHECKED = 'checked', 'unchecked'
    CHECKED_CHOICES = (
        (CHECKED, 'Проверен'),
        (UNCHECKED, 'Не проверен'),
    )
    is_checked = models.CharField('Статус',max_length=20,choices=CHECKED_CHOICES, default=UNCHECKED)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def image_tag_small(self):
        return '<img src="https://school-s3-assets.s3.amazonaws.com/media/{0}" style="width:100px;height:100px;">'.format(self.img)

    def image_tag(self):
        return '<img src="https://school-s3-assets.s3.amazonaws.com/media/{0}" style="width:500px;">'.format(self.img)

    image_tag.short_description = 'Фото'
    image_tag.allow_tags = True

    image_tag_small.short_description = 'Фото'
    image_tag_small.allow_tags = True

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Фотоотчет'
        verbose_name_plural = 'Фотоотчеты'
