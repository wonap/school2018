# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-06 17:50
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('photorepost', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='photorepost',
            options={'verbose_name': 'Фотоотчет', 'verbose_name_plural': 'Фотоотчеты'},
        ),
        migrations.AddField(
            model_name='photorepost',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='photorepost',
            name='comment',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='photorepost',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='photorepost',
            name='published_date',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True),
        ),
        migrations.AddField(
            model_name='photorepost',
            name='rating',
            field=models.IntegerField(default=0),
        ),
    ]
