from django.apps import AppConfig


class PhotorepostConfig(AppConfig):
    name = 'photorepost'
    verbose_name = 'Фотоотчеты'
