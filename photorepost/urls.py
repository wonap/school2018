from django.conf.urls import url
from . import views


app_name = 'photorepost'
urlpatterns = [
    url(r'^$', views.PhotorepostListView.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', views.PhotorepostDetailView.as_view(), name='detail'),
    url(r'^create/$', views.PhotoRepostCreateView.as_view(), name='create'),
    url(r'^result/$', views.PhotoRepostResultView.as_view(), name='result'),
    url(r'^(?P<pk>[0-9]+)/update/$', views.PhotoRepostUpdateView.as_view(), name='update'),
]


