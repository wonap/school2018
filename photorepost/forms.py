from django import forms
from django.core.exceptions import ValidationError
from django.conf import settings
from .models import Photorepost


class PhotorepostForm(forms.ModelForm):

    class Meta:
        model = Photorepost
        fields = ('img', 'title', 'text',
                  'type_food','treatment','water',
                  'breakfast','one_snack','lunch',
                  'second_snack','dinner','author')

    def clean_img(self):
        avatar = self.cleaned_data.get('img', False)
        if avatar:
            if avatar.size > settings.MAX_UPLOAD_SIZE:
                raise ValidationError('Изображение больше 4 мегабайтов')
            return avatar
