"""school URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from payment import views

urlpatterns = [
                  url(r'^admin/', admin.site.urls),
                  # url(r'^registration/$', TemplateView.as_view(template_name="registration/reg.html")),
                  url(r'^registration/$', views.TestView.as_view()),
                  url(r'^about/$', TemplateView.as_view(template_name="registration/about.html")),
                  url(r'^faq/$', TemplateView.as_view(template_name="faq.html")),
                  url(r'^login/$', auth_views.login, name='login'),
                  url(r'^logout/$', auth_views.logout, name='logout'),
                  url(r'password_change/$', auth_views.password_change,
                      {'template_name': 'registration/change_password.html'},
                      name='password-change'),
                  url(r'password_reset/$', auth_views.password_reset,
                      {'template_name': 'registration/password_reset.html'},
                      name='password-reset'),
                  url(r'^password_reset_done/$', auth_views.password_reset_done,
                      {'template_name': 'registration/password_reset_done.html'},
                      name='password_reset_done'),

                  url(r'^password_reset_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
                      auth_views.password_reset_confirm,
                      {'post_reset_redirect': '/password_done/'}, name='password_reset_confirm'),

                  url(r'^password_done/$', auth_views.password_reset_complete),

                  url(r'^password-change-done/$', auth_views.password_change_done,
                      {'template_name': 'registration/password_change_done.html'},
                      name='password_change_done'),
                  url(r'^ckeditor/', include('ckeditor_uploader.urls')),
                  url(r'', include('news.urls')),
                  url(r'^articles/', include('articles.urls')),
                  url(r'^chat/', include('chat.urls')),
                  url(r'^training/', include('training.urls')),
                  url(r'^photorepost/', include('photorepost.urls')),
                  url(r'^food/', include('food.urls')),
                  url(r'^quiz/', include('quiz.urls')),
                  url(r'^users/', include('users.urls')),
                  url(r'^payment/', include('payment.urls')),
                  url(r'^support/', include('supportapp.urls')),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
