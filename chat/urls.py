from django.conf.urls import url
from . import views

app_name = 'chat'
urlpatterns = [
    url(r'^$', views.MessageListView.as_view(), name='list'),
    url(r'^testmessage/$', views.MessageListTestView.as_view(), name='test-list'),
    url(r'^error/$', views.MessageErrorView.as_view(), name='error'),
    url(r'^consultant-room/(?P<pk>[0-9]+)$', views.MessageRoomView.as_view(), name='consultant-room'),
    url(r'^tbase-room/(?P<pk>[0-9]+)$', views.TBaseMessageRoomView.as_view(), name='tbase-room'),
    url(r'^search/$', views.UserSearchView.as_view(), name='search'),
]
