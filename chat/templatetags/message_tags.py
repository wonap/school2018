from django import template
from decimal import Decimal as D

from chat.models import Message, NewMessage

register = template.Library()


@register.filter()
def last_message_time(user, consultant):
    message = Message.objects.filter(sender__in=[user, consultant],
                                          recipent__in=[user, consultant]).order_by('-created').first()
    if message:
        return message.created
    else:
        return ''

@register.filter()
def is_new_messages_for_user(user, consultant):
    count_messages = NewMessage.objects.filter(user=user, sender=consultant).count()
    return count_messages
