from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views.generic import ListView, View, TemplateView
from django.core.urlresolvers import reverse_lazy
from common.mixins import PayCheck
from django.db.models import Q
import datetime
from itertools import chain
from datetime import date, timedelta
from django.core.paginator import (Paginator, EmptyPage,
                                   PageNotAnInteger, InvalidPage,
                                   )

from chat.forms import MessageCreateForm
from chat.models import Message, NewMessage
from users.models import User


class MessageListView(LoginRequiredMixin, PayCheck,View):

    def get_message_user_ids(self,user):
        ids = Message.objects.values_list('sender_id', 'recipent_id'). \
            filter(Q(recipent_id=user.pk) | Q(sender_id=user.pk))
        users_ids = []
        for msg in ids:
            a, b = msg
            if a != user.pk:
                users_ids.append(a)
            if b != user.pk:
                users_ids.append(b)
        return users_ids

    def get(self, request, *args, **kwargs):
        template_name = 'chat/message_list.html'
        if request.user.is_consultant_manager():
            users_ids = self.get_message_user_ids(request.user)
            users = User.objects.filter(id__in=users_ids)
        elif request.user.is_base_user():
            users = User.objects.filter(type=User.ASSISTENT)
        elif request.user.is_food_user():
            users = User.objects.filter(type=User.ASSISTENT)
        elif request.user.is_children_user():
            users = User.objects.filter(type__in=[User.ZUBAREVA, User.PEDIATRICIAN, User.GV])
        elif request.user.is_food_plus_sport_user():
            users = User.objects.filter(type__in=[User.ASSISTENT, User.COACH, User.NEUROLOGIST])
        elif request.user.is_food_plus_specialist_user():
            users = User.consultants_objects.filter(~Q(type__in=[User.ASSISTENT, User.GV, User.COACH, User.PEDIATRICIAN, User.ZUBAREVA, User.GASTRO]))
        elif request.user.is_family_user():
            users = User.consultants_objects.filter(~Q(type__in=[User.ASSISTENT, User.GV, User.DIETOLOG, User.GASTROTWO]))
        else:
            users = User.consultants_objects.filter(~Q(type__in=[User.ASSISTENT, User.GV, User.PEDIATRICIAN, User.DIETOLOG, User.GASTROTWO]))
        message_counts = NewMessage.objects.filter(user=request.user).count()
        ctx = dict()
        ctx['message_counts'] = message_counts
        ctx['consultants'] = users
        return render(request, template_name, ctx)


class MessageListTestView(LoginRequiredMixin,View):

    def get_message_user_ids(self,user):
        ids = set(list(Message.objects.values_list('sender_id','recipent_id'). \
            filter(Q(recipent_id=user.pk) | Q(sender_id=user.pk))))
        users_ids = []
        for msg in ids:
            a,b = msg
            if a != user.pk:
                users_ids.append(a)
            if b != user.pk:
                users_ids.append(b)
        return users_ids

    # def get_message_consultant_ids(self,):

    def get(self, request, *args, **kwargs):
        page = self.request.GET.get('page', None)
        template_name = 'chat/message_test_list.html'
        if request.user.is_consultant_manager():
            users_ids = self.get_message_user_ids(request.user)
            users = User.objects.filter(id__in=users_ids)
        elif request.user.is_food_user():
            users = User.objects.filter(type=User.ASSISTENT)
        elif request.user.is_children_user():
            users = User.objects.filter(type__in=[User.ZUBAREVA, User.PEDIATRICIAN, User.GV])
        elif request.user.is_food_plus_sport_user():
            users = User.objects.filter(type__in=[User.ASSISTENT, User.COACH, User.NEUROLOGIST])
        elif request.user.is_family_user():
            users = User.consultants_objects.filter(~Q(type__in=[User.ASSISTENT, User.GV]))
        else:
            users = User.consultants_objects.filter(~Q(type__in=[User.ASSISTENT, User.GV, User.PEDIATRICIAN]))
        message_counts = NewMessage.objects.filter(user=request.user).count()
        new_consultants = User.objects.filter(id__in=list(request.user.new_messages.values_list('sender_id', flat=True)))
        consultants = users.exclude(id__in=new_consultants)

        # new_consultants = new_consultants.order_by('sender_messages__created')
        all_users = list(chain(new_consultants, consultants))

        paginator = Paginator(all_users, 50)
        try:
            consultants = paginator.page(page)
        except PageNotAnInteger:
            consultants = paginator.page(1)
        except EmptyPage:
            consultants = paginator.page(paginator.num_pages)
        ctx = dict()
        ctx['message_counts'] = message_counts
        ctx['consultants'] = consultants
        # ctx['new_consultants'] = new_consultants
        return render(request, template_name, ctx)


class MessageRoomView(LoginRequiredMixin,View):
    template_name = 'chat/message_open.html'

    def get(self, request, *args, **kwargs):
        consultant = User.objects.get(pk=kwargs.get('pk'))
        messages = Message.objects.filter(sender__in=[request.user, consultant],
                                          recipent__in=[request.user, consultant]).\
            order_by('created')

        NewMessage.objects.filter(message__in=messages, user=request.user).delete()
        message_form = MessageCreateForm()
        ctx = {}
        ctx['consultant'] = consultant
        ctx['messages'] = messages
        ctx['message_form'] = message_form
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        today = date.today()
        print("today", today)
        count_messages = Message.objects.filter(sender=request.user, recipent_id=request.POST.get('recipent'), created__date=today).count()
        print(count_messages)
        if count_messages < 3:
            message_form = MessageCreateForm(request.POST, request.FILES)
            if message_form.is_valid():
                message = message_form.save()
                new_message = NewMessage()
                new_message.message = message
                new_message.user = message.recipent
                new_message.sender = message.sender
                new_message.save()
                pk = kwargs.get('pk')
                to_redirect = reverse_lazy('chat:consultant-room', kwargs={'pk': pk})
                return redirect(to_redirect)
        else:
            return render(request, 'chat/message_error.html')



class TBaseMessageRoomView(LoginRequiredMixin,View):
    template_name = 'chat/message_open.html'

    def get(self, request, *args, **kwargs):
        consultant = User.objects.get(pk=kwargs.get('pk'))
        messages = Message.objects.filter(sender__in=[request.user, consultant],
                                          recipent__in=[request.user, consultant]).\
            order_by('created')

        NewMessage.objects.filter(message__in=messages, user=request.user).delete()
        message_form = MessageCreateForm()
        ctx = {}
        ctx['consultant'] = consultant
        ctx['messages'] = messages
        ctx['message_form'] = message_form
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        today = date.today()
        print("today", today)
        count_messages = Message.objects.filter(sender=request.user, recipent_id=request.POST.get('recipent'), created__date=today).count()
        print(count_messages)
        if count_messages < 1:
            message_form = MessageCreateForm(request.POST, request.FILES)
            if message_form.is_valid():
                message = message_form.save()
                new_message = NewMessage()
                new_message.message = message
                new_message.user = message.recipent
                new_message.sender = message.sender
                new_message.save()
                pk = kwargs.get('pk')
                to_redirect = reverse_lazy('chat:tbase-room', kwargs={'pk': pk})
                return redirect(to_redirect)
        else:
            return render(request, 'chat/message_error.html')




class MessageErrorView(LoginRequiredMixin,TemplateView):
    template_name = 'chat/message_error.html'



class UserSearchView(ListView):
    model = Message
    template_name = "users_search.html"



    def get(self, request, *args, **kwargs):
        ctx = dict()
        question = request.GET.get('q')
        if question is not None:
            users = User.objects.filter(Q(first_name__icontains=question) | Q(last_name__icontains=question))

        ctx['users'] = users
        return render(request, self.template_name, ctx)
