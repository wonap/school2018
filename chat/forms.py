from django import forms

from chat.models import Message


class MessageCreateForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('sender', 'recipent', 'text', 'img')

