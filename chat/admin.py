from django.contrib import admin

from .models import Message, NewMessage


class MessageAdmin(admin.ModelAdmin):
    list_display = ('text', 'created', 'sender_last_name','sender_first_name', 'recipent',)
    list_filter = ('created',)
    search_fields = ['recipent__email','text']
    date_hierarchy = 'created'


    def sender_last_name(self, obj):
        return obj.sender.last_name

    def sender_first_name(self, obj):
        return obj.sender.first_name

admin.site.register(Message, MessageAdmin)
admin.site.register(NewMessage)
