from django.db import models
from django.utils import timezone

from users.models import User


class Message(models.Model):
    text = models.TextField()
    img = models.ImageField(upload_to='message', null=True, blank=True)
    created = models.DateTimeField(default=timezone.now)
    sender = models.ForeignKey(User, related_name='sender_messages')
    recipent = models.ForeignKey(User, related_name='recipent_messages')
    private_type = models.BooleanField(default=False)
    is_delete = models.BooleanField(default=False)
    is_new = models.BooleanField(default=True)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class NewMessage(models.Model):
    user = models.ForeignKey(User, related_name='new_messages')
    sender = models.ForeignKey(User,related_name='new_sender_messages')
    message = models.ForeignKey(Message, related_name='new_messages')

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Новые сообщения'
        verbose_name_plural = 'Новые сообщения'
