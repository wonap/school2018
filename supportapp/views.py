from django.core.mail import EmailMessage
from django.shortcuts import render, redirect
from django.template import Context
from django.template.loader import get_template
from django.views.generic import ListView, View, TemplateView
from .forms import ContactForm

def contact(request):
    form_class = ContactForm



    # new logic!
    if request.method == 'POST':
        form = form_class(data=request.POST)


        if form.is_valid():
            contact_name = request.POST.get(
                'contact_name'
            , '')

            contact_email = request.user

            form_content = request.POST.get('content', '')

            # Email the profile with the
            # contact information
            template = get_template('contact_template.txt')
            context = Context({
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            })
            content = template.render(context)


            email = EmailMessage(
                "Обратная связь",
                content,
                contact_email,
                ['school@zooparkshop.ru'],
                headers = {'Reply-To': contact_email }
            )
            email.send()
            return redirect('support:thank')

    return render(request, 'contact.html', {
        'form': form_class,
    })

