from django.conf.urls import url
from . import views
from django.views.generic import TemplateView


app_name = 'support'
urlpatterns = [
    url(r'^$', views.contact, name='item'),
    url(r'^thank/', TemplateView.as_view(template_name="thank.html"), name='thank'),
]
