from django.contrib import admin
from common.models import Week, KnowledgeLevel, Tariff, MainSettings


class WeekAdmin(admin.ModelAdmin):
    list_display = ('name','begin', 'end')


class TariffAdmin(admin.ModelAdmin):
    list_display = ('name','code', 'cost')


class MainSettingsAdmin(admin.ModelAdmin):
    list_display = ('name','code', 'value')


admin.site.register(Week, WeekAdmin)
admin.site.register(MainSettings, MainSettingsAdmin)
admin.site.register(Tariff, TariffAdmin)
admin.site.register(KnowledgeLevel)
