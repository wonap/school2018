import datetime
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.utils import timezone
from payment.models import PaymentCondition, PaymentTinkofCondition
from users.models import User


class PayCheck(object):

    def dispatch(self, request, *args, **kwargs):
        rates = ['food', 'food_plus_sport',
                 'food_plus_specialist','pregnancy',
                 'all_inclusive', 'family', 'children', 'base'
        ]

        if User.objects.filter(pk=request.user.pk,groups__name__in=rates).exists():
            pc = PaymentCondition.objects.filter(user=request.user).first()

            new_date_users = PaymentCondition.objects.filter(user=request.user).exclude(date_created__gte=datetime.date(2018, 3, 31)).first()

            if not pc:
                # return redirect(reverse('payment:prolong'))
                return redirect(reverse('payment:tinkof-prolong'))
            elif not pc.payd:
                return redirect(reverse('payment:tinkof-oplata',kwargs={'pk':request.user.pk}))
                # return redirect(reverse('payment:pay-tariff',kwargs={'pk':request.user.pk}))
            elif not new_date_users:
                return render(request, 'new_users.html')

        return super().dispatch(request, *args, **kwargs)
