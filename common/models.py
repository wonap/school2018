from django.db import models
from django.utils import timezone


class MainSettings(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)
    value = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Настройки'
        verbose_name_plural = 'Настройки'


class Week(models.Model):
    name = models.CharField(max_length=100)
    begin = models.DateTimeField(default=timezone.now)
    end = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Неделя'
        verbose_name_plural = 'Недели'


class KnowledgeLevel(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Уровень знаний'
        verbose_name_plural = 'Уровень знаний'


class Tariff(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)
    cost = models.DecimalField(max_digits=10, decimal_places=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тариф'
        verbose_name_plural = 'Тарифы'


class Season(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
