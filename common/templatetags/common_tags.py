from django import template
from decimal import Decimal as D

from chat.models import Message, NewMessage
from news.models import NewsNewComment
register = template.Library()


@register.filter()
def to_int(value):
    return int(value)


@register.inclusion_tag("templatetags/count_messages.html", takes_context=True)
def count_messages(context):
    current_user = context['user']
    # message_counts = Message.objects.filter(recipent=current_user, is_new=True).count()
    message_counts = NewMessage.objects.filter(user=current_user).count()
    return {
        'counts': message_counts,
    }


def get_tariff_name():
    pass



@register.inclusion_tag("templatetags/count_comments.html", takes_context=True)
def count_comments(context):
    current_user = context['user']
    comments_counts = NewsNewComment.objects.filter(parent=current_user).count()

    return {
        'comments_counts': comments_counts,
    }